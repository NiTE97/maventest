<h1>Maven Test App</h1>

Befehl zum Compilen:<br>
mvn compile<br>
mvn clean install<br>
<br>
Befehl zum Ausfuehren:<br>
java -cp target/maventest-1.0-SNAPSHOT.jar progmvn.App<br>
<br>
In der pom.xml kann die Java-Version entsprechend angepasst werden.
	
